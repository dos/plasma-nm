# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Sönke Dibbern <s_dibbern@web.de>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-22 00:49+0000\n"
"PO-Revision-Date: 2014-08-13 22:07+0200\n"
"Last-Translator: Sönke Dibbern <s_dibbern@web.de>\n"
"Language-Team: Low Saxon <kde-i18n-nds@kde.org>\n"
"Language: nds\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 1.5\n"

#: bluetoothmonitor.cpp:67
#, kde-format
msgid "Only 'dun' and 'nap' services are supported."
msgstr "Bloots „DUN“- un „NAP”-Deensten warrt ünnerstütt."

#: connectivitymonitor.cpp:61
#, kde-format
msgid "Network authentication"
msgstr ""

#: connectivitymonitor.cpp:68
#, kde-format
msgid "Log in"
msgstr ""

#: connectivitymonitor.cpp:71
#, kde-format
msgid "You need to log into this network"
msgstr ""

#: connectivitymonitor.cpp:91
#, kde-format
msgid "Limited Connectivity"
msgstr ""

#: connectivitymonitor.cpp:92
#, kde-format
msgid ""
"This device appears to be connected to a network but is unable to reach the "
"internet."
msgstr ""

#: modemmonitor.cpp:181
#, kde-format
msgctxt "Text in GSM PIN/PUK unlock error dialog"
msgid "Error unlocking modem: %1"
msgstr "Fehler bi't Opsluten vun't Modem: %1"

#: modemmonitor.cpp:182
#, kde-format
msgctxt "Title for GSM PIN/PUK unlock error dialog"
msgid "PIN/PUK unlock error"
msgstr "Fehler bi't Opsluten mit PIN oder PUK"

#: notification.cpp:97
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to ConfigFailedReason"
msgid "The device could not be configured"
msgstr "De Reedschap lett sik nich inrichten"

#: notification.cpp:100
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to "
"ConfigUnavailableReason"
msgid "IP configuration was unavailable"
msgstr "Keen IP-Instellen verföögbor"

#: notification.cpp:103
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to ConfigExpiredReason"
msgid "IP configuration expired"
msgstr "IP-Instellen aflopen"

#: notification.cpp:106
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to NoSecretsReason"
msgid "No secrets were provided"
msgstr "Keen Anmellinformatschonen praatstellt"

#: notification.cpp:109
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to "
"AuthSupplicantDisconnectReason"
msgid "Authorization supplicant disconnected"
msgstr "Identiteetprööv-Praatsteller afkoppelt"

#: notification.cpp:113
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to "
"AuthSupplicantConfigFailedReason"
msgid "Authorization supplicant's configuration failed"
msgstr "Instellen vun den Identiteetprööv-Praatsteller fehlslaan"

#: notification.cpp:116
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to "
"AuthSupplicantFailedReason"
msgid "Authorization supplicant failed"
msgstr "Identiteetprööv-Praatstellen fehlslaan"

#: notification.cpp:119
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to "
"AuthSupplicantTimeoutReason"
msgid "Authorization supplicant timed out"
msgstr "Tietafloop bi de Identiteetprööv-Praatstellen"

#: notification.cpp:122
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to PppStartFailedReason"
msgid "PPP failed to start"
msgstr "PPP lett sik nich starten"

#: notification.cpp:125
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to PppDisconnectReason"
msgid "PPP disconnected"
msgstr "PPP afkoppelt"

#: notification.cpp:128
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to PppFailedReason"
msgid "PPP failed"
msgstr "PPP fehlslaan"

#: notification.cpp:131
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to DhcpStartFailedReason"
msgid "DHCP failed to start"
msgstr "DHCP lett sik nich starten"

#: notification.cpp:134
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to DhcpErrorReason"
msgid "A DHCP error occurred"
msgstr "Dat geev en DHCP-Fehler"

#: notification.cpp:137
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to DhcpFailedReason"
msgid "DHCP failed "
msgstr "DHCP fehlslaan "

#: notification.cpp:140
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to "
"SharedStartFailedReason"
msgid "The shared service failed to start"
msgstr "De deelt Deenst lett sik nich starten"

#: notification.cpp:143
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to SharedFailedReason"
msgid "The shared service failed"
msgstr "De deelt Deenst is fehlslaan"

#: notification.cpp:146
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to "
"AutoIpStartFailedReason"
msgid "The auto IP service failed to start"
msgstr "De Auto-IP-Deenst lett sik nich starten"

#: notification.cpp:149
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to AutoIpErrorReason"
msgid "The auto IP service reported an error"
msgstr "De Auto-IP-Deenst hett en Fehler torüchgeven"

#: notification.cpp:152
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to AutoIpFailedReason"
msgid "The auto IP service failed"
msgstr "De Auto-IP-Deenst funkscheneert nich"

#: notification.cpp:155
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to ModemBusyReason"
msgid "The modem is busy"
msgstr "Dat Modem hett to doon"

#: notification.cpp:158
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to ModemNoDialToneReason"
msgid "The modem has no dial tone"
msgstr "Dat Modem hett keen Freetoon"

#: notification.cpp:161
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to ModemNoCarrierReason"
msgid "The modem shows no carrier"
msgstr "Dat Modem wiest keen Dreegsignaal"

#: notification.cpp:164
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to "
"ModemDialTimeoutReason"
msgid "The modem dial timed out"
msgstr "De Inwahltiet för't Modem is aflopen"

#: notification.cpp:167
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to ModemDialFailedReason"
msgid "The modem dial failed"
msgstr "Mit dat Modem hett dat Inwählen nich funkscheneert"

#: notification.cpp:170
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to ModemInitFailedReason"
msgid "The modem could not be initialized"
msgstr "Dat Modem lett sik nich torechtmaken"

#: notification.cpp:173
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to "
"GsmApnSelectFailedReason"
msgid "The GSM APN could not be selected"
msgstr "De GSM-APN lett sik nich utsöken"

#: notification.cpp:176
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to GsmNotSearchingReason"
msgid "The GSM modem is not searching"
msgstr "Dat GSM-Modem söcht nich"

#: notification.cpp:179
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to "
"GsmRegistrationDeniedReason"
msgid "GSM network registration was denied"
msgstr "De GSM-Nettwark-Inmellen wöör afwiest"

#: notification.cpp:182
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to "
"GsmRegistrationTimeoutReason"
msgid "GSM network registration timed out"
msgstr "Bi de GSM-Nettwark-Inmellen is de Tiet aflopen"

#: notification.cpp:185
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to "
"GsmRegistrationFailedReason"
msgid "GSM registration failed"
msgstr "GSM-Inmellen is fehlslaan"

#: notification.cpp:188
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to "
"GsmPinCheckFailedReason"
msgid "The GSM PIN check failed"
msgstr "De GSM-PIN-Prööv is fehlslaan"

#: notification.cpp:191
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to FirmwareMissingReason"
msgid "Device firmware is missing"
msgstr "De Reedschap-Firmware fehlt"

#: notification.cpp:194
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to DeviceRemovedReason"
msgid "The device was removed"
msgstr "De Reedschap wöör wegmaakt"

#: notification.cpp:197
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to SleepingReason"
msgid "The networking system is now sleeping"
msgstr "Dat Nettwarksysteem slöppt nu"

#: notification.cpp:200
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to "
"ConnectionRemovedReason"
msgid "The connection was removed"
msgstr "De Verbinnen wöör wegmaakt"

#: notification.cpp:205
#, kde-format
msgctxt "@info:status Notification when the device failed due to CarrierReason"
msgid "The cable was disconnected"
msgstr "Dat Kavel wöör aftrocken"

#: notification.cpp:211
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to ModemNotFoundReason"
msgid "The modem could not be found"
msgstr "Dat Modem lett sik nich finnen"

#: notification.cpp:214
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to BluetoothFailedReason"
msgid "The bluetooth connection failed or timed out"
msgstr "De Bluetooth-Verbinnen is fehlslaan oder ehr Tiet is aflopen"

#: notification.cpp:217
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to GsmSimNotInserted"
msgid "GSM Modem's SIM Card not inserted"
msgstr "Binnen dat GSM-Modem stickt keen SIM-Koort."

#: notification.cpp:220
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to GsmSimPinRequired"
msgid "GSM Modem's SIM Pin required"
msgstr "PIN vun't GSM-Modem sien SIM-Koort nödig"

#: notification.cpp:223
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to GsmSimPukRequired"
msgid "GSM Modem's SIM Puk required"
msgstr "PUK vun't GSM-Modem sien SIM-Koort nödig"

#: notification.cpp:226
#, kde-format
msgctxt "@info:status Notification when the device failed due to GsmSimWrong"
msgid "GSM Modem's SIM wrong"
msgstr "SIM vun't GSM-Modem is leeg"

#: notification.cpp:229
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to InfiniBandMode"
msgid "InfiniBand device does not support connected mode"
msgstr "InfiniBand-Reedschap ünnerstütt Verbinnen-Bedriefoort nich"

#: notification.cpp:232
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to DependencyFailed"
msgid "A dependency of the connection failed"
msgstr "En Vörutsetten för de Verbinnen is fehlslaan"

#: notification.cpp:235
#, kde-format
msgctxt "@info:status Notification when the device failed due to Br2684Failed"
msgid "Problem with the RFC 2684 Ethernet over ADSL bridge"
msgstr "Dat gifft en Problem mit de „Ethernet over ADSL“-Brüch (RFC 2684)"

#: notification.cpp:238
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to "
"ModemManagerUnavailable"
msgid "ModemManager not running"
msgstr "„ModemManager“ löppt nich"

#: notification.cpp:241
#, kde-format
msgctxt "@info:status Notification when the device failed due to SsidNotFound"
msgid "The WiFi network could not be found"
msgstr "Dat Funknettwark lett sik nich finnen"

#: notification.cpp:245
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to "
"SecondaryConnectionFailed"
msgid "A secondary connection of the base connection failed"
msgstr "De Tweetverbinnen blang de Basisverbinnen funkscheneert nich"

#: notification.cpp:248
#, kde-format
msgctxt "@info:status Notification when the device failed due to DcbFcoeFailed"
msgid "DCB or FCoE setup failed"
msgstr ""

#: notification.cpp:251
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to TeamdControlFailed"
msgid "teamd control failed"
msgstr ""

#: notification.cpp:254
#, kde-format
msgctxt "@info:status Notification when the device failed due to ModemFailed"
msgid "Modem failed or no longer available"
msgstr ""

#: notification.cpp:257
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to ModemAvailable"
msgid "Modem now ready and available"
msgstr ""

#: notification.cpp:260
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to SimPinIncorrect"
msgid "The SIM PIN was incorrect"
msgstr ""

#: notification.cpp:263
#, fuzzy, kde-format
#| msgctxt ""
#| "@info:status Notification when the device failed due to "
#| "ConnectionRemovedReason"
#| msgid "The connection was removed"
msgctxt "@info:status Notification when the device failed due to NewActivation"
msgid "A new connection activation was enqueued"
msgstr "De Verbinnen wöör wegmaakt"

#: notification.cpp:266
#, fuzzy, kde-format
#| msgctxt ""
#| "@info:status Notification when the device failed due to "
#| "DeviceRemovedReason"
#| msgid "The device was removed"
msgctxt "@info:status Notification when the device failed due to ParentChanged"
msgid "The device's parent changed"
msgstr "De Reedschap wöör wegmaakt"

#: notification.cpp:269
#, kde-format
msgctxt ""
"@info:status Notification when the device failed due to ParentManagedChanged"
msgid "The device parent's management changed"
msgstr ""

#: notification.cpp:345
#, kde-format
msgid "Connection '%1' activated."
msgstr "Verbinnen „%1“ anmaakt"

#: notification.cpp:372
#, kde-format
msgid "Connection '%1' deactivated."
msgstr "Verbinnen „%1“ utmaakt"

#: notification.cpp:423
#, kde-format
msgid "VPN connection '%1' activated."
msgstr "VPN-Verbinnen „%1“ anmaakt"

#: notification.cpp:426
#, fuzzy, kde-format
#| msgid "VPN connection '%1' activated."
msgid "VPN connection '%1' failed to activate."
msgstr "VPN-Verbinnen „%1“ anmaakt"

#: notification.cpp:429 notification.cpp:437
#, fuzzy, kde-format
#| msgid "VPN connection '%1' activated."
msgid "VPN connection '%1' deactivated."
msgstr "VPN-Verbinnen „%1“ anmaakt"

#: notification.cpp:440
#, fuzzy, kde-format
#| msgid ""
#| "The VPN connection changed state because the device it was using was "
#| "disconnected."
msgid ""
"VPN connection '%1' was deactivated because the device it was using was "
"disconnected."
msgstr ""
"De VPN-Verbinnen hett ehr Tostand ännert, ehr bruukt Reedschap wöör "
"afkoppelt."

#: notification.cpp:443
#, fuzzy, kde-format
#| msgid "The service providing the VPN connection was stopped."
msgid "The service providing the VPN connection '%1' was stopped."
msgstr "De Deenst, de de VPN-Verbinnen praatstellt, wöör anhollen."

#: notification.cpp:446
#, fuzzy, kde-format
#| msgid "The IP config of the VPN connection was invalid."
msgid "The IP config of the VPN connection '%1', was invalid."
msgstr "De VPN-Verbinnen ehr IP-Instellen sünd leeg"

#: notification.cpp:449
#, kde-format
msgid "The connection attempt to the VPN service timed out."
msgstr "Bi't Tokoppeln na den VPN-Deenst is de Tiet aflopen"

#: notification.cpp:452
#, fuzzy, kde-format
#| msgid ""
#| "A timeout occurred while starting the service providing the VPN "
#| "connection."
msgid ""
"A timeout occurred while starting the service providing the VPN connection "
"'%1'."
msgstr ""
"Bi't Starten vun den Deenst, de de VPN-Verbinnen praatstellt, is de Tiet "
"aflopen"

#: notification.cpp:455
#, fuzzy, kde-format
#| msgid "Starting the service providing the VPN connection failed."
msgid "Starting the service providing the VPN connection '%1' failed."
msgstr "Starten vun den Deenst, de de VPN-Verbinnen praatstellt, is fehlslaan"

#: notification.cpp:458
#, fuzzy, kde-format
#| msgid "Necessary secrets for the VPN connection were not provided."
msgid "Necessary secrets for the VPN connection '%1' were not provided."
msgstr "De nödigen Anmelldaten för de VPN-Verbinnen wöörn nich praatstellt"

#: notification.cpp:461
#, kde-format
msgid "Authentication to the VPN server failed."
msgstr "Identiteetprööv op den VPN-Server is fehlslaan"

#: notification.cpp:464
#, fuzzy, kde-format
#| msgctxt ""
#| "@info:status Notification when the device failed due to "
#| "ConnectionRemovedReason"
#| msgid "The connection was removed"
msgid "The connection was deleted."
msgstr "De Verbinnen wöör wegmaakt"

#: notification.cpp:550
#, kde-format
msgid "No Network Connection"
msgstr ""

#: notification.cpp:551
#, kde-format
msgid "You are no longer connected to a network."
msgstr ""

#: passworddialog.cpp:54
#, kde-format
msgid "Authenticate %1"
msgstr ""

#: passworddialog.cpp:98
#, fuzzy, kde-format
#| msgid "Please provide the password for activating connection '%1'"
msgid "Provide the password for the wireless network '%1':"
msgstr "Giff bitte dat Passwoort för't Anmaken vun de Verbinnen „%1“ in"

#: passworddialog.cpp:100
#, fuzzy, kde-format
#| msgid "Please provide the password for activating connection '%1'"
msgid "Provide the password for the connection '%1':"
msgstr "Giff bitte dat Passwoort för't Anmaken vun de Verbinnen „%1“ in"

#: passworddialog.cpp:106
#, fuzzy, kde-format
#| msgid "Password dialog"
msgid "%1 password dialog"
msgstr "Passwoort-Dialoog"

#: passworddialog.cpp:126
#, fuzzy, kde-format
#| msgid "Please provide the password for activating connection '%1'"
msgid "Provide the secrets for the VPN connection '%1':"
msgstr "Giff bitte dat Passwoort för't Anmaken vun de Verbinnen „%1“ in"

#: passworddialog.cpp:127
#, fuzzy, kde-format
#| msgid "VPN secrets (%1)"
msgid "VPN secrets (%1) dialog"
msgstr "VPN-Anmelldaten (%1)"

#. i18n: ectx: property (windowTitle), widget (QWidget, PasswordDialog)
#: passworddialog.ui:26
#, kde-format
msgid "Password dialog"
msgstr "Passwoort-Dialoog"

#. i18n: ectx: property (text), widget (QLabel, labelPass)
#: passworddialog.ui:92
#, kde-format
msgid "Password:"
msgstr "Passwoort:"

#: pindialog.cpp:65
#, kde-format
msgid "SIM PUK"
msgstr "SIM-PUK"

#: pindialog.cpp:67
#, kde-format
msgid "SIM PUK2"
msgstr "SIM-PUK2"

#: pindialog.cpp:69
#, kde-format
msgid "Service provider PUK"
msgstr "Deenstanbeder-PUK"

#: pindialog.cpp:71
#, kde-format
msgid "Network PUK"
msgstr "Nettwark-PUK"

#: pindialog.cpp:73
#, kde-format
msgid "Corporate PUK"
msgstr "Firmen-PUK"

#: pindialog.cpp:75
#, kde-format
msgid "PH-FSIM PUK"
msgstr "PH-FSIM-PUK"

#: pindialog.cpp:77
#, kde-format
msgid "Network Subset PUK"
msgstr "Ünnernettwark-PUK"

#: pindialog.cpp:80 pindialog.cpp:112
#, kde-format
msgid "%1 unlock required"
msgstr "Opsluten mit %1 deit noot"

#: pindialog.cpp:81 pindialog.cpp:113
#, kde-format
msgid "%1 Unlock Required"
msgstr "Opsluten mit %1 deit noot"

#: pindialog.cpp:82 pindialog.cpp:114
#, kde-format
msgid ""
"The mobile broadband device '%1' requires a %2 code before it can be used."
msgstr ""
"Ehr sik de mobile Breedbandreedschap „%1“ bruken lett, deit de %2-Kode noot."

#: pindialog.cpp:83 pindialog.cpp:115
#, kde-format
msgid "%1 code:"
msgstr "%1-Kode:"

#. i18n: ectx: property (text), widget (QLabel, pinLabel)
#: pindialog.cpp:84 pinwidget.ui:153
#, kde-format
msgid "New PIN code:"
msgstr "Nieg PIN-Kode:"

#: pindialog.cpp:85
#, kde-format
msgid "Re-enter new PIN code:"
msgstr "Nieg PIN-Kode nochmaal ingeven:"

#. i18n: ectx: property (text), widget (QCheckBox, chkShowPass)
#: pindialog.cpp:86 pinwidget.ui:193
#, kde-format
msgid "Show PIN/PUK code"
msgstr "PIN-/PUK-Kode wiesen"

#: pindialog.cpp:96
#, kde-format
msgid "SIM PIN"
msgstr "SIM-PIN"

#: pindialog.cpp:98
#, kde-format
msgid "SIM PIN2"
msgstr "SIM-PIN2"

#: pindialog.cpp:100
#, kde-format
msgid "Service provider PIN"
msgstr "Deenstanbeder-PIN"

#: pindialog.cpp:102
#, kde-format
msgid "Network PIN"
msgstr "Nettwark-PIN"

#: pindialog.cpp:104
#, kde-format
msgid "PIN"
msgstr "PIN"

#: pindialog.cpp:106
#, kde-format
msgid "Corporate PIN"
msgstr "Firmen-PIN"

#: pindialog.cpp:108
#, kde-format
msgid "PH-FSIM PIN"
msgstr "PH-FSIM-PIN"

#: pindialog.cpp:110
#, kde-format
msgid "Network Subset PIN"
msgstr "Ünnernettwark-PIN"

#: pindialog.cpp:116
#, kde-format
msgid "Show PIN code"
msgstr "PIN-Kode wiesen"

#: pindialog.cpp:198
#, kde-format
msgid "PIN code too short. It should be at least 4 digits."
msgstr "PIN-Kode is to kort. Dat mööt tominnst veer Steden wesen."

#: pindialog.cpp:203
#, kde-format
msgid "The two PIN codes do not match"
msgstr "De twee PIN-Koden sünd nich liek"

#: pindialog.cpp:208
#, kde-format
msgid "PUK code too short. It should be 8 digits."
msgstr "PUK-Kode is to kort. Dat mööt tominnst acht Steden wesen."

#: pindialog.cpp:213
#, kde-format
msgid "Unknown Error"
msgstr "Nich begäng Fehler"

#. i18n: ectx: property (windowTitle), widget (QWidget, PinWidget)
#. i18n: ectx: property (text), widget (QLabel, title)
#: pinwidget.ui:14 pinwidget.ui:47
#, kde-format
msgid "SIM PIN unlock required"
msgstr "Opsluten mit SIM-PIN deit noot"

#. i18n: ectx: property (text), widget (QLabel, prompt)
#: pinwidget.ui:76
#, no-c-format, kde-format
msgid ""
"The mobile broadband device '%1' requires a SIM PIN code before it can be "
"used."
msgstr ""
"Ehr sik de mobile Breedbandreedschap „%1“ bruken lett, deit en SIM-PIN-Kode "
"noot."

#. i18n: ectx: property (text), widget (QLabel, pukLabel)
#: pinwidget.ui:133
#, fuzzy, kde-format
#| msgid "PUK code:"
msgid "PUK code:"
msgstr "PUK-Kode:"

#. i18n: ectx: property (text), widget (QLabel, pin2Label)
#: pinwidget.ui:173
#, kde-format
msgid "Re-enter PIN code:"
msgstr "PIN-Kode nochmaal ingeven:"

#: secretagent.cpp:405
#, fuzzy, kde-format
#| msgid "Authentication to the VPN server failed."
msgid "Authentication to %1 failed. Wrong password?"
msgstr "Identiteetprööv op den VPN-Server is fehlslaan"

#~ msgid "VPN connection '%1' failed."
#~ msgstr "VPN-Verbinnen „%1“ fehlslaan"

#~ msgid "VPN connection '%1' disconnected."
#~ msgstr "VPN-Verbinnen „%1“ afkoppelt"

#~ msgid "The VPN connection changed state because the user disconnected it."
#~ msgstr ""
#~ "De VPN-Verbinnen hett ehr Tostand ännert, de Bruker hett ehr afkoppelt."

#~ msgid "The connection was deleted from settings."
#~ msgstr "De Verbinnen wöör ut de Instellen wegmaakt"

#, fuzzy
#~| msgid ""
#~| "For accessing the wireless network '%1' you need to provide a password "
#~| "below"
#~ msgid ""
#~ "For accessing the wireless network %1 you need to provide a password "
#~ "below:"
#~ msgstr ""
#~ "För den Togriep op't Funknettwark „%1“ muttst Du nerrn en Passwoort "
#~ "ingeven"

#, fuzzy
#~| msgid ""
#~| "For accessing the wireless network '%1' you need to provide a password "
#~| "below"
#~ msgid ""
#~ "For accessing the vpn connection %1 you need to provide secrets below:"
#~ msgstr ""
#~ "För den Togriep op't Funknettwark „%1“ muttst Du nerrn en Passwoort "
#~ "ingeven"

#~ msgid "TextLabel"
#~ msgstr "Textfeld"

#~ msgid "Show password"
#~ msgstr "Passwoort wiesen"

#, fuzzy
#~| msgid "New PIN code:"
#~ msgid "&New PIN code:"
#~ msgstr "Nieg PIN-Kode:"

#~ msgid "Could not contact Bluetooth manager (BlueZ)."
#~ msgstr "De Bluetooth-Pleger („BlueZ“) lett sik nich ansnacken."

#~ msgid "%1 (%2) does not support Dialup Networking (DUN)."
#~ msgstr "%1 (%2) ünnerstütt keen Inwähl-Nettwark (DUN)"

#~ msgid "%1 (%2) does not support Network Access Point (NAP)."
#~ msgstr "%1 (%2) ünnerstütt keen Nettwark-Togangpunkt („NAP“)."

#~ msgid "Default Bluetooth adapter not found: %1"
#~ msgstr "Standard-Bluetooth-Koort lett sik nich finnen: %1"

#~ msgid "Error activating devices's serial port: %1"
#~ msgstr "Fehler bi't Anmaken vun de Reedschappen ehr seriell Koppelsteed: %1"

#~ msgid "Device %1 is not the wanted one (%2)"
#~ msgstr "Reedschap „%1“ is nich de bruukte („%2“)"

#~ msgid "Device for serial port %1 (%2) not found."
#~ msgstr "Reedschap för seriell Koppelsteed %1 (%2) lett sik nich finnen."
