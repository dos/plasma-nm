# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2017, 2019, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-01-02 01:06+0000\n"
"PO-Revision-Date: 2022-12-21 16:42+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.0\n"

#: kcm.cpp:344
#, kde-format
msgid "my_shared_connection"
msgstr "mijn_gedeelde_verbinding"

#: kcm.cpp:416
#, kde-format
msgid "Export VPN Connection"
msgstr "VPN-verbinding exporteren"

#: kcm.cpp:441
#, kde-format
msgid "Do you want to save changes made to the connection '%1'?"
msgstr "Wilt u gemaakte wijzigen aan de verbinding '%1' opslaan?"

#: kcm.cpp:442
#, kde-format
msgctxt "@title:window"
msgid "Save Changes"
msgstr "Wijzigingen opslaan"

#: kcm.cpp:531
#, kde-format
msgid "Import VPN Connection"
msgstr "VPN-verbinding importeren"

#: kcm.cpp:531
#, kde-format
msgid "VPN connections (%1)"
msgstr "VPN-verbindingen (%1)"

#: qml/AddConnectionDialog.qml:15
msgctxt "@title:window"
msgid "Choose a Connection Type"
msgstr "Een verbindingstype kiezen"

#: qml/AddConnectionDialog.qml:163
msgid "Create"
msgstr "Aanmaken"

#: qml/AddConnectionDialog.qml:174 qml/ConfigurationDialog.qml:112
msgid "Cancel"
msgstr "Annuleren"

#: qml/AddConnectionDialog.qml:191 qml/main.qml:193
msgid "Configuration"
msgstr "Configuratie"

#: qml/ConfigurationDialog.qml:16
msgctxt "@title:window"
msgid "Configuration"
msgstr "Configuratie"

#: qml/ConfigurationDialog.qml:37
msgid "General"
msgstr "Algemeen"

#: qml/ConfigurationDialog.qml:42
msgid "Ask for PIN on modem detection"
msgstr "Om PIN vragen bij detectie van modem"

#: qml/ConfigurationDialog.qml:49
msgid "Show virtual connections"
msgstr "Virtuele verbindingen tonen"

#: qml/ConfigurationDialog.qml:57
msgid "Hotspot"
msgstr "Hotspot"

#: qml/ConfigurationDialog.qml:63
msgid "Hotspot name:"
msgstr "Hotspotnaam:"

#: qml/ConfigurationDialog.qml:73
msgid "Hotspot password:"
msgstr "Hotspotwachtwoord:"

#: qml/ConfigurationDialog.qml:102
msgid "OK"
msgstr "OK"

#: qml/ConnectionItem.qml:108
msgid "Connect"
msgstr "Verbinden"

#: qml/ConnectionItem.qml:108
msgid "Disconnect"
msgstr "Verbinding verbreken"

#: qml/ConnectionItem.qml:121
msgid "Delete"
msgstr "Verwijderen"

#: qml/ConnectionItem.qml:131
msgid "Export"
msgstr "Exporteren"

#: qml/ConnectionItem.qml:141
msgid "Connected"
msgstr "Verbonden"

#: qml/ConnectionItem.qml:143
msgid "Connecting"
msgstr "Bezig met verbinden"

#: qml/main.qml:139
msgid "Add new connection"
msgstr "Nieuwe verbinding toevoegen"

#: qml/main.qml:153
msgid "Remove selected connection"
msgstr "Geselecteerde verbinding verwijderen"

#: qml/main.qml:169
msgid "Export selected connection"
msgstr "Geselecteerde verbinding exporteren"

#: qml/main.qml:221
msgctxt "@title:window"
msgid "Remove Connection"
msgstr "Verbinding verwijderen"

#: qml/main.qml:222
msgid "Do you want to remove the connection '%1'?"
msgstr "Wilt u de verbinding '%1 verwijderen?"

#~ msgid "Ok"
#~ msgstr "OK"

#~ msgid "Search…"
#~ msgstr "Zoeken…"

#~ msgid "Type here to search connections..."
#~ msgstr "Hier invoeren om verbindingen te zoeken..."

#~ msgid "Close"
#~ msgstr "Sluiten"

#~ msgid "Form"
#~ msgstr "Formulier"
